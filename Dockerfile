FROM php:7.2.0-apache

MAINTAINER Florindo Lopez

# PHP Mail settings
ENV PHP_MAIL_IP='mailcatcher' \
	PHP_MAIL_PORT='25' \
#	PHP_MAIL_USERNAME='' \
#	PHP_MAIL_PASSWORD='' \
#	PHP_MAIL_USE_SSL='no' \
	PHP_MAIL_USE_TLS='no'

#	PHP_XDEBUG
ENV PHP_XDEBUG_ENABLE='Off' \
	PHP_XDEBUG_REMOTE_HOST='10.0.75.1' \
	PHP_OPCACHE_ENABLE='On' \
	PHP_ZLIB_OUTPUT_COMPRESSION_ENABLE='On' \
	PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL='-1'

# Set locale to ES to be able to use month names in PHP for the locale
RUN apt-get update -qq \
	&& apt-get -y -qq install apt-utils rsyslog locales cron \
	&& sed -i 's/^# *\(es_ES.UTF-8\)/\1/' /etc/locale.gen \
	&& sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
	&& sed -i 's/^# *\(cron.*\)/\1/' /etc/rsyslog.conf \
	&& service rsyslog restart \
	&& service cron start \
	&& locale-gen

# OS variables
ENV DEBIAN_FRONTEND='noninteractive' \
	TZ='Europe/Amsterdam' \
	LANG='es_ES.UTF-8' \
	LANGUAGE='es_ES.UTF-8' \
	LC_ALL='es_ES.UTF-8' \
	TERM='xterm'

RUN apt-get -y -q -o=Dpkg::Use-Pty=0 install \
		git \
		zip \
		unzip \
		ssmtp \
		wget \
		unixodbc-dev \
		imagemagick \
		libfreetype6-dev \
		libjpeg-dev \
		# libmcrypt-dev \
		# libpng12-dev \
		# libmemcached-dev \
		libmagickwand-dev \
		libmagickcore-dev \
		zlib1g-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* \
	# && pecl install memcached-2.2.0 \
    && printf "\n" | pecl install imagick \
	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install -j$(nproc) zip iconv mysqli sockets gd opcache pdo_mysql \
	# && docker-php-ext-enable memcached \
    && docker-php-ext-enable imagick \

	# install PHPUnit: https://phpunit.de/
	&& wget -q https://phar.phpunit.de/phpunit-5.7.phar \
	&& chmod +x phpunit-5.7.phar \
	&& mv phpunit-5.7.phar /usr/local/bin/phpunit \

	# Enable mod_rewrite for Apache
	&& a2enmod -q rewrite expires headers deflate \

	# install composer
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer